const User = require('../models/User')
const userController = {
  userList: [
    { id: 1, name: 'DARK GEAR', gender: 'M' },
    { id: 2, name: 'JAEMS DARK', gender: 'F' }

  ],
  lastId: 3,
  async addUser (req, res, next) {
    const payload = req.body
    // res.json(userController.addUser(payload))
    const user = new User(payload)
    try {
      await user.save(user)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateUser (req, res, next) {
    const payload = req.body
    // res.json(userController.updateUser(payload))
    try {
      const user = await User.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser (req, res, next) {
    const { id } = req.params
    // res.json(userController.deleteUser(id))
    try {
      const user = await User.deleteOne({ _id: id })
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUsers (req, res, next) {
    try {
      const users = await User.find({})
      res.json(users)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async getUser  (req, res, next) {
    const { id } = req.params
    try {
      const users = await User.findById(id)
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  }

}

module.exports = userController
