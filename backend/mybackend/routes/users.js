const express = require('express')
const userController = require('../controller/UserController')
// const User = require('../models/User')
const router = express.Router()

/* GET users listing. */
router.get('/', userController.getUsers)

router.get('/:id', userController.getUser)

router.post('/', userController.addUser)

router.put('/', userController.updateUser)

router.delete('/:id', userController.deleteUser)

module.exports = router
