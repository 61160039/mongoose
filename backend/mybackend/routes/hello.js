const express = require('express')
const router = express.Router()

router.get('/:message', function (req, res, next) {
  const { params } = req
  res.json({ message: 'Hello My name is James.', params })
})

module.exports = router
