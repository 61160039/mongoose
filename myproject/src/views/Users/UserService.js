const userService = {
  userList: [{ id: 1, name: 'DARK GEAR', gender: 'M' }, { id: 2, name: 'JAEMS DARK', gender: 'W' }],
  lastId: 1,
  addUser (user) {
    user.id = this.lastId++
    this.userList.push(user)
  },
  updateUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
  },
  deleteUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1)
  },
  getUser () {
    return [...this.userList]
  }
}

export default userService
